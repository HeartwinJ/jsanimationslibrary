import { Component } from "react";
import { Row, Col } from 'react-bootstrap';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { vscDarkPlus } from "react-syntax-highlighter/dist/esm/styles/prism";

export default class AnimationContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      code: 'loading...'
    }
  }

  async componentDidMount() {
    await fetch('scripts/script-' + (this.props.containerIndex + 1) + '.js')
      .then(response => response.text())
      .then(response => this.setState({code: response}))

    let container = document.getElementById('canvasContainer-' + this.props.containerIndex)
    let scriptElem = document.createElement('script')
    scriptElem.setAttribute('src', 'scripts/' + this.props.animation)
    scriptElem.setAttribute('type', 'module')
    let frameBody = container.contentWindow.document.getElementsByTagName('body')[0]
    frameBody.appendChild(scriptElem)
  }

  render() {
    return (
      <div className="mx-5">
        <Row>
          <Col xs={{span: 12, order: 'last'}} lg={{span: 6, order: 'first'}}>
            <SyntaxHighlighter language={'javascript'} style={vscDarkPlus}>
              {this.state.code}
            </SyntaxHighlighter>
          </Col>
          <Col className="text-center">
            <iframe
              className="canvasContainer rounded-3"
              id={'canvasContainer-' + this.props.containerIndex}
              title={'canvasContainer-' + this.props.containerIndex}
              width="100%"
              height="400px"
            ></iframe>
          </Col>
        </Row>
      </div>
    )
  }
}