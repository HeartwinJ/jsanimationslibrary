import { Component } from "react";
import { Container } from "react-bootstrap";
import AnimationContainer from "./animationContainer";

export default class App extends Component {
  render() {
    let animations = [
      'script-1.js',
      // 'script-2.js'
    ]
    return (
      <Container>
        <div className="p-5">
          <h1>JS Canvas Animations</h1>
        </div>
        <div>
          {
            animations.map((animation, index) => {
              return <AnimationContainer animation={animation} key={index} containerIndex={index}></AnimationContainer>
            })
          }
        </div>
      </Container>
    )
  }
}