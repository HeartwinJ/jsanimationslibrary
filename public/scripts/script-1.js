import Size from './models/size.js'
import Position from './models/position.js'

let canvas, ctx, akatsukiImg, resizedImg1
const imgSize1 = new Size(100, 15500 / 199), imgSize2 = new Size(50, 7750 / 199)

function onResize(evt) {
  canvas.height = evt.target.innerHeight
  canvas.width = evt.target.innerWidth
}

function drawImage(ctx, position, size) {
  ctx.drawImage(akatsukiImg, position.x, position.y, size.width, size.height)
}

function init() {
  let styles = document.createElement('style')
  styles.innerHTML = 'html, body { margin: 0; padding: 0; }'
  document.head.appendChild(styles)

  canvas = document.createElement('canvas')
  ctx = canvas.getContext('2d')

  canvas.width = document.body.scrollWidth
  canvas.height = document.body.scrollHeight
  document.body.appendChild(canvas)

  akatsukiImg = new Image()
  akatsukiImg.src = '/assets/akatsuki.png'
  
  resizedImg1 = document.createElement('canvas')
  const Img1Ctx = resizedImg1.getContext('2d')
  resizedImg1.width = imgSize1.width + 40
  resizedImg1.height = imgSize1.height + 40
  
  window.addEventListener('resize', onResize)
  
  akatsukiImg.onload = function () {
    Img1Ctx.drawImage(akatsukiImg, 20, 20, imgSize1.width - 20, imgSize1.height - 20)
    requestAnimationFrame(loop)
  }
}

function loop() {
  ctx.fillStyle = '#000'
  ctx.fillRect(0, 0, canvas.width, canvas.height)

  // drawImage(ctx, new Position(10, 10), imgSize1)
  // drawImage(ctx, new Position(10, 150), imgSize1)
  // drawImage(ctx, new Position(10, 290), imgSize1)

  // drawImage(ctx, new Position(160, 10), imgSize1)
  // drawImage(ctx, new Position(160, 150), imgSize1)
  // drawImage(ctx, new Position(160, 290), imgSize1)

  // drawImage(ctx, new Position(320, 10), imgSize1)
  // drawImage(ctx, new Position(320, 150), imgSize1)
  // drawImage(ctx, new Position(320, 290), imgSize1)

  // drawImage(ctx, new Position(480, 10), imgSize1)
  // drawImage(ctx, new Position(480, 150), imgSize1)
  // drawImage(ctx, new Position(480, 290), imgSize1)

  let testPattern = ctx.createPattern(resizedImg1, 'repeat')
  ctx.fillStyle = testPattern
  ctx.fillRect(0, 0, canvas.width, canvas.height)

  requestAnimationFrame(loop)
}

init()